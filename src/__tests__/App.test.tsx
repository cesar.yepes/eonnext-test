import 'react-native';
import React from 'react';
import {render} from '@testing-library/react-native';
import App from '../App';

describe('App main file', () => {
  jest.useFakeTimers();

  test('renders correctly', () => {
    expect(render(<App />)).toMatchSnapshot();
  });
});
