import React, {FC} from 'react';
import {
  Text as RNText,
  TextStyle,
  TextProps as RNTextProps,
  StyleProp,
} from 'react-native';
import {styles as fonts, TypographyTypes} from '../../../theme/fonts';
import style from './style';

interface TypographyProps extends RNTextProps {
  type?: TypographyTypes;
  testID?: string;
  children: React.ReactNode;
  style?: StyleProp<TextStyle>;
}

const Typography: FC<TypographyProps> = props => {
  const {
    children,
    testID,
    type = 'body',
    style: customStyle,
    numberOfLines,
  } = props;

  return (
    <RNText
      testID={testID}
      style={[style.text, fonts[type], customStyle]}
      numberOfLines={numberOfLines}>
      {children}
    </RNText>
  );
};

export default Typography;
