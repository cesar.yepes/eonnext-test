import React from 'react';
import {text, withKnobs} from '@storybook/addon-knobs';
import {storiesOf} from '@storybook/react-native';
import {StyleSheet, View, Linking} from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import Typography from './index';

const style = StyleSheet.create({
  center: {
    marginHorizontal: 10,
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  separator: {
    marginVertical: 10,
  },
});

storiesOf('Navigation', module)
  .addDecorator(withKnobs)
  .add('Text Link', () => {
    const to = text('URL To', 'http://');
    const textLink = text('Link Text 2', 'Link Text 2');
    const textLinkBig = text('Link Text 2 Big', 'Link Text 2 Big');
    return (
      <ScrollView>
        <View style={style.center}>
          <TouchableOpacity onPress={() => Linking.openURL(to)}>
            <Typography type="linkText">{textLink}</Typography>
          </TouchableOpacity>
          <View style={style.separator} />
          <TouchableOpacity onPress={() => Linking.openURL(to)}>
            <Typography type="linkTextBig">{textLinkBig}</Typography>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  });
