import {StyleSheet} from 'react-native';
import {colors} from '../../../theme/colors';

export default StyleSheet.create({
  text: {
    color: colors.primary.purple.darkPurple,
  },
});
