import 'react-native';
import React from 'react';
import {render} from '@testing-library/react-native';
import Typography from '../Typography';

describe('Typography component', () => {
  test('renders correctly', () => {
    expect(render(<Typography>Test</Typography>)).toMatchSnapshot();
  });
});
