import {scale} from 'react-native-size-matters/extend';
import {colors} from './colors';

export const family = {
  HaasDisplay: {
    Bold: 'HaasGrotDispR-75Bold',
    BoldItalic: 'HaasGrotDispR-76BoldItalic',
    Roman: 'HaasGrotDispR-55Roman',
    Italic: 'HaasGrotDispR-56Italic',
  },
  HaasText: {
    Bold: 'HaasGrotTextR-75Bold',
    BoldItalic: 'HaasGrotTextR-76BoldItalic',
    Roman: 'HaasGrotTextR-55Roman',
    Italic: 'HaasGrotTextR-56Italic',
  },
};

export const styles = {
  h1: {
    fontFamily: family.HaasDisplay.Bold,
    fontSize: scale(28),
    lineHeight: scale(48),
  },
  h2: {
    fontFamily: family.HaasDisplay.Bold,
    fontSize: scale(20),
    lineHeight: scale(32),
  },
  h3: {
    fontFamily: family.HaasDisplay.Bold,
    fontSize: scale(14),
    lineHeight: scale(24),
  },
  h4: {
    fontFamily: family.HaasDisplay.Bold,
    fontSize: scale(12),
    lineHeight: scale(18),
  },
  body: {
    fontFamily: family.HaasDisplay.Roman,
    fontSize: scale(18),
    lineHeight: scale(32),
  },
  bodyText: {
    fontFamily: family.HaasText.Roman,
    fontSize: scale(18),
    lineHeight: scale(32),
  },
  primaryButton: {
    fontFamily: family.HaasText.Bold,
    fontSize: scale(18),
    lineHeight: scale(24),
  },
  secondaryButton: {
    fontFamily: family.HaasText.Roman,
    fontSize: scale(18),
    lineHeight: scale(24),
  },
  caption: {
    fontFamily: family.HaasText.Roman,
    fontSize: scale(12),
    lineHeight: scale(18),
  },
  footerLink: {
    fontFamily: family.HaasText.Roman,
    fontSize: scale(16),
    lineHeight: scale(24),
    letterSpacing: scale(1),
    textTransform: 'uppercase' as const,
  },
  linkTextBig: {
    color: colors.primary.purple.lightPurple,
    fontFamily: family.HaasDisplay.Roman,
    fontSize: scale(22),
    lineHeight: scale(32),
    textDecorationLine: 'underline' as const,
  },
  linkText: {
    color: colors.primary.purple.lightPurple,
    fontFamily: family.HaasDisplay.Roman,
    fontSize: scale(18),
    lineHeight: scale(32),
    textDecorationLine: 'underline' as const,
  },
};

export type TypographyTypes = keyof typeof styles;
