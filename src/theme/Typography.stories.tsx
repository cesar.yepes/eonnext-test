import {storiesOf} from '@storybook/react-native';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Text from '../common/components/Typography';

const style = StyleSheet.create({
  center: {
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center',
  },
  container: {
    marginHorizontal: 10,
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const typography = [
  {
    text: 'H1 Display',
    type: 'h1',
  },
  {
    text: 'H2 Display',
    type: 'h2',
  },
  {
    text: 'H3 Display',
    type: 'h3',
  },
  {
    text: 'H4 Display',
    type: 'h4',
  },
  {
    text: 'Body Display 1',
    type: 'body',
  },
  {
    text: 'Body Text 2',
    type: 'bodyText',
  },
  {
    text: 'Primary Button Text',
    type: 'primaryButton',
  },
  {
    text: 'Secondary Button Text',
    type: 'secondaryButton',
  },
  {
    text: 'Caption Text',
    type: 'caption',
  },
  {
    text: 'Footer Link Uppercase Text',
    type: 'footerLink',
  },
  {
    text: 'Link Text 2',
    type: 'linkTextBig',
  },
  {
    text: 'Link Text 2',
    type: 'linkText',
  },
];

storiesOf('Branding Identity', module).add('Typography', () => (
  <ScrollView>
    <View style={style.center}>
      {typography.map(item => (
        <View key={item.type} style={style.container}>
          <Text type={item.type}>{item.text}</Text>
        </View>
      ))}
    </View>
  </ScrollView>
));
