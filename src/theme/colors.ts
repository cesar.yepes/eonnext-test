export const colors = {
  primary: {
    eonNextRed: '#FF4822',
    purple: {
      lightPurple: '#9400FF',
      purple: '#5E0D98',
      darkPurple: '#36164A',
    },
    birch: {
      birch: '#FAF6F4',
      darkBirch: '#F6F0ED',
    },
  },
  tints: {
    purple: {
      grayPurpleDark: '#5D426B',
      grayPurpleMedium: '#7E6787',
      grayPurpleLight: '#AF9EB1',
      grayPurpleVeryLight: '#DFD3D9',
    },
  },
  character: {
    cloud: '#00E3FF',
    leaf: '#1AE570',
    sun: '#FFDE00',
    flash: '#FF83FF',
    gasOff: '#9400FF',
    gasOn: '#FF4722',
  },
  white: '#FFFFFF',
  black: '#000000',
  transparent: 'transparent',
};
