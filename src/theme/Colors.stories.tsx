import {storiesOf} from '@storybook/react-native';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {colors} from './colors';

const style = StyleSheet.create({
  center: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignContent: 'center',
    justifyContent: 'center',
  },
  container: {
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  circle: {
    marginVertical: 10,
    width: 66,
    height: 66,
    borderRadius: 33,
  },
});

const colours = [
  {
    color: colors.primary.eonNextRed,
    text: 'E.ON Next Red',
  },
  {
    color: colors.primary.purple.lightPurple,
    text: 'Light Purple',
  },
  {
    color: colors.primary.purple.purple,
    text: 'Purple',
  },
  {
    color: colors.primary.purple.darkPurple,
    text: 'Dark Purple',
  },
  {
    color: colors.primary.birch.birch,
    text: 'Birch',
  },
  {
    color: colors.primary.birch.darkBirch,
    text: 'Dark Birch',
  },
  {
    color: colors.tints.purple.grayPurpleDark,
    text: 'Gray Purple Dark',
  },
  {
    color: colors.tints.purple.grayPurpleMedium,
    text: 'Gray Purple Medium',
  },
  {
    color: colors.tints.purple.grayPurpleLight,
    text: 'Gray Purple Light',
  },
  {
    color: colors.tints.purple.grayPurpleVeryLight,
    text: 'Gray Purple Very Light',
  },
  {
    color: colors.character.cloud,
    text: 'Cloud',
  },
  {
    color: colors.character.leaf,
    text: 'Leaf',
  },
  {
    color: colors.character.sun,
    text: 'Sun',
  },
  {
    color: colors.character.flash,
    text: 'Flash',
  },
  {
    color: colors.character.gasOff,
    text: 'Gas - Off State',
  },
  {
    color: colors.character.gasOn,
    text: 'Gas - On state',
  },
  {
    color: colors.white,
    text: 'Mouth',
  },
  {
    color: colors.black,
    text: 'Facial Feature',
  },
];

storiesOf('Branding Identity', module).add('Colours', () => (
  <ScrollView>
    <View style={style.center}>
      {colours.map(item => (
        <View key={item.text} style={style.container}>
          <View style={[style.circle, {backgroundColor: item.color}]} />
          <Text>{item.text}</Text>
          <Text>{item.color}</Text>
        </View>
      ))}
    </View>
  </ScrollView>
));
