import React, {FC} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import LoggedOutStack from './LoggedOutStack';

const Navigation: FC = () => (
  <NavigationContainer>
    <LoggedOutStack />
  </NavigationContainer>
);

export default Navigation;
