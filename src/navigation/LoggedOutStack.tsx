import React, {FC} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import OnBoardingScreen from '../screens/OnBoarding';
import LoginScreen from '../screens/Login';
import routes from './routes';

const Stack = createStackNavigator();

const LoggedOutStack: FC = () => (
  <Stack.Navigator>
    <Stack.Screen
      name={routes.LOGIN}
      options={{title: 'Login'}}
      component={LoginScreen}
    />
    <Stack.Screen
      name={routes.ON_BOARDING}
      options={{title: 'On Boarding'}}
      component={OnBoardingScreen}
    />
  </Stack.Navigator>
);

export default LoggedOutStack;
