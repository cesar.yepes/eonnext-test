import {gql, useMutation} from '@apollo/client';
import {useNavigation} from '@react-navigation/native';
import React, {FC, useState} from 'react';
import {Button, SafeAreaView, StyleSheet, Text, TextInput} from 'react-native';
import routes from '../../navigation/routes';

// TODO remove after testing
// This is a test code to verify (QA) the connection with the graphql API, this code must be removed after test
const QUERY = gql`
  mutation emailPasswordAuthentication($email: String!, $password: String!) {
    emailAuthentication(email: $email, password: $password) {
      token
    }
  }
`;

const style = StyleSheet.create({
  container: {flex: 1, alignItems: 'center', justifyContent: 'center'},
});

const LoginScreen: FC = () => {
  const navigation = useNavigation();
  const [login, {data}] = useMutation(QUERY);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [state, setState] = useState('idle');

  const loginPress = async () => {
    try {
      setState('loading');
      await login({
        variables: {
          email,
          password,
        },
      });
      setState('completed');
    } catch (e) {
      setState('rejected');
      setError(e);
    }
  };

  // TODO implement real functionality after a user's successful login
  const navigateToOnboarding = () => {
    navigation.navigate(routes.ON_BOARDING);
  };

  return (
    <SafeAreaView style={style.container}>
      <Text>Login Screen</Text>
      <Text>State: {state}</Text>
      {state === 'rejected' && <Text>{JSON.stringify(error)}</Text>}
      {state === 'completed' ? (
        <Text>{JSON.stringify(data)}</Text>
      ) : (
        <>
          <TextInput
            placeholder="email"
            value={email}
            onChangeText={setEmail}
          />
          <TextInput
            placeholder="password"
            value={password}
            secureTextEntry
            onChangeText={setPassword}
          />
          <Button title="login" onPress={loginPress} />
        </>
      )}
      <Button
        title="Navigate to Onboarding Screen"
        onPress={navigateToOnboarding}
      />
    </SafeAreaView>
  );
};
export default LoginScreen;
