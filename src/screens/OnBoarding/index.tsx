import {createSelector} from '@reduxjs/toolkit';
import React, {FC} from 'react';
import {SafeAreaView, StyleSheet, Text, Button} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {demoAction} from '../../actions/session';
import {RootState} from '../../store';

const style = StyleSheet.create({
  container: {flex: 1, alignItems: 'center', justifyContent: 'center'},
});

// TODO redux test code delete after test
const testSelector = createSelector(
  [({session}: RootState) => session],
  session => session.testValue,
);

const DemoRedux = () => {
  const testValue = useSelector(testSelector);
  const dispatch = useDispatch();

  const triggerDemoReduxAction = () => {
    dispatch(demoAction(1));
  };

  return (
    <>
      <Text>Redux test value: {testValue}</Text>
      <Button onPress={triggerDemoReduxAction} title="Test redux action +1" />
    </>
  );
};

const OnBoardingScreen: FC = () => (
  <SafeAreaView style={style.container}>
    <Text>On Boarding Screen</Text>
    <DemoRedux />
  </SafeAreaView>
);

export default OnBoardingScreen;
