import {API_URL} from '@env';
import {ApolloClient, InMemoryCache, createHttpLink} from '@apollo/client';

const link = createHttpLink({
  uri: API_URL,
});

const client = new ApolloClient({
  link,
  cache: new InMemoryCache(),
});

export default client;
