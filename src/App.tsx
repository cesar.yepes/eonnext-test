import React, {FC} from 'react';
import {Provider} from 'react-redux';
import {ApolloProvider} from '@apollo/client';
import store from './store';
import Navigation from './navigation';
import apolloClient from './apolloClient';

const App: FC = () => (
  <ApolloProvider client={apolloClient}>
    <Provider store={store}>
      <Navigation />
    </Provider>
  </ApolloProvider>
);

export default App;
