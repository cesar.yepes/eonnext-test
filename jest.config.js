const transformPackages = [
  '@react-native',
  'react-native',
  'react-navigation',
  '@react-navigation/.*',
];

module.exports = {
  preset: 'react-native',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  collectCoverageFrom: [
    './src/**/*.{ts,tsx,js,jsx}',
    '!./src/**/*.stories.{ts,tsx,js,jsx}',
    '!./src/**/*.d.ts',
    '!./src/Storybook.ts',
  ],
  collectCoverage: true,
  coverageReporters: ['html'],
  coverageThreshold: {
    global: {
      statements: 90,
      branches: 90,
      functions: 90,
      lines: 90,
    },
  },
  testRegex: '(src/__tests__/.*|\\.(test|spec))\\.(ts|tsx|js)$',
  transformIgnorePatterns: [`node_modules/(?!${transformPackages.join('|')})`],
  setupFiles: ['./__tests__/setup.ts'],
};
