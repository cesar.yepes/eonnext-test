import {AppRegistry} from 'react-native';
import Storybook from './src/Storybook';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Storybook);
